package ee.valiit.chat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;

@SpringBootApplication
public class ChatApplication implements CommandLineRunner {

            // KODUTÖÖ.
            // 1. Profiili pilt uesti korda teha
            // Random kanal peab uuesti töötama. Peab vahetama toa ära.

	public static void main(String[] args) {

		SpringApplication.run(ChatApplication.class, args);
	}

	@Autowired          // Ühendab ära klassi Jdbc dependency muutuja nimega jdbc//
		JdbcTemplate jdbcTemplate;

	@Override
	public void run(String... args) throws Exception {
		System.out.println("CONFIGURE Database Tables");
		jdbcTemplate.execute("DROP TABLE IF EXISTS messages");
		jdbcTemplate.execute("CREATE TABLE messages (id SERIAL, username TEXT, message TEXT, avatar TEXT, room TEXT)");
	}
}
