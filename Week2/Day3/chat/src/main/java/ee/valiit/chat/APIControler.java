package ee.valiit.chat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;


@RestController // aitab ise kontrollida programmi korrektsust
@CrossOrigin    // lubab igat tüüpi päringuid

public class APIControler {

    @Autowired          // Ühendab ära klassi Jdbc dependency muutuja nimega jdbc
            JdbcTemplate jdbcTemplate;

    public APIControler() {

    }

    @GetMapping("/chat/{room}")
        // Kutsub välja messages SELECT * FROM messages
    ArrayList<ChatMessage> chat(@PathVariable String room) {

        try {                  // Võib juhtuda RACE CONDITION !!! Cant indentifify messages !
            String sqlKask = "SELECT * FROM messages WHERE room= '"+room+"'";
            ArrayList<ChatMessage> messages = (ArrayList) jdbcTemplate.query(sqlKask, (resultSet, rowNum) -> {
                String username = resultSet.getString("username");
                String message = resultSet.getString("message");
                String avatar = resultSet.getString("avatar");
                return new ChatMessage(username, message, avatar);
            });
            return messages;  // Võib juhtuda RACE CONDITION !!! Cant indentifify messages !
        } catch (DataAccessException err) {
            System.out.println("TABLE WAS NOT READY");
            return new ArrayList();
        }
    }

    @PostMapping("chat/{room}/new-message")
        // Postitab meassage
    void newMessage(@RequestBody ChatMessage msg, @PathVariable String room) {
        String sqlKask = "INSERT INTO messages (username, message, avatar, room) VALUES  ('" +
                                                            msg.getUsername()+ "','"+
                                                            msg.getMessage()+ "', '"+
                                                            msg.getAvatar()+ "', '"+
                                                            room + "')";
        jdbcTemplate.execute(sqlKask);

    }


}

