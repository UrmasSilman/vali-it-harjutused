import java.math.BigInteger;


public class Main {
    public static void main(String[] args) {

        System.out.println(täisarv(3)); // sout (täisarv(number));
        System.out.println(täisarv(78));
        System.out.println(täisarv(6));

        test2("sisend1", "sisend2"); // Test2 harjutus. Stringi sisend on vaja sisestada " märkide vahel "
        test2("p", "t");

        System.out.println(12.0);
        addVat(12.0);                 // Test3, teine osa
        System.out.println(addVat(12.0));

        int c = 5;
        int d = 6;
        System.out.println(Meetod(c, d)); // Test 4

        String gender = deriveGender("37007014254"); // Test5
        System.out.println("Gender: " + gender);

        printHello(); // Test 6

        int synniAasta = deriveBirthYear("37007014254"); // Test7
        System.out.println("Sünniaasta on: " + synniAasta);

        BigInteger isikukoodnr = new BigInteger("37002070261"); // Test8
        boolean nimi = validatePersonalCode(isikukoodnr);
        System.out.println(nimi);


        System.out.println("Hello world!!");
    }
    //T est1 Defineeri meetod nimega test1, mis võtab sisendparameetrina täisarvu ja 
    // tagastab tõeväärtuse
    // Kutsu see meetod main() meetodist välja

    public static boolean täisarv(int value) {
        if (value > 0) {
            return true;
        } else {
            return false;
        }

    }
    // Test2 Defineeri meetod nimega test2, millel on kaks String-tüüpi sisendparameetrit ja 
    // mis ei tagasta mitte midagi
    // Kutsu see meetod main() meetodist välja

    public static void test2(String a, String b) {

    }


    //Test3 Defineeri meetod nimega addVat, mis võtab sisendparameetrina ühe double tüüpi muutuja ja 
    // tagastab double tüüpi muutuja
    // Kutsu see meetod main() meetodist välja
    public static double addVat(double muutuja) {
        muutuja = muutuja * 1.2;
        return muutuja;
    }

    // Test4  Derfineeri meetod, mis võtab sisendparameetitena kaks täisarvu ja ühe tõeväärtuse 
    // ning tagastab täisarvude massiivi
    // Nime võid ise välja mõelda
    public static int[] Meetod(int a, int b) {
        int i = 8;
        int[] mas1 = new int[i];
        return mas1;
    }

    // Test5 Defineeri meetod dervieGender, mis võtab sisendparameetrina vastu Eesti iskukoodi 
    // teksti kujul ja tgastab
    // kõnealuse isikukoodi omaniku soo teksti kujul (Kas "M" või "F").
    // Kutsu see meetod main() meetodist välja ja talleta tulemus lokaalse main() meetodi muutujas 
    // nimega Gender.
    // Prindi selle muutuja väärtus standardväljundisse.

    public static String deriveGender(String isikukood) {
        if (isikukood.startsWith("3") || isikukood.startsWith("5")) {
            return "M";
        } else {
            return "F";
        }

    }
    // Test6 Defineeri meetod printHello, mis ei võta vastu ühtegi sisendparameetrit ning 
    // mis ei tagasta ühtgi väärtust
    // aga prindib standardväljundisse "Tere"
    // Kutsu see meetod main() meetodist välja

    public static void printHello() {
        System.out.println("Tere");
    }


    // Test7 Defineeri meetod deriveBirthYear, mis võtab sisendparameetrina vastu 
    // eesti iskukoodi tekstikujul ja
    // tagastab selle isikukoodi omaniku sünniaasta täisarvulisel kujul.
    // Kutsu see meetod main() meetodist välja ja prindi tulemus stabndardväljundisse.

    public static int deriveBirthYear(String isikukood) { // substring võtab Stringist välja 2 ja 3 char'i,
        String aastaStr = isikukood.substring(1, 3);      // 70, 3 näitab, et sealt lõikab ära.
        int aasta = Integer.parseInt(aastaStr);  // Integer.parseInt(aastaStr) muudab Stringi int väärtuseks 
        aasta = 1900 + aasta;
        return aasta;

    }


    // Test8 Defineeri meetod validatePersonalCode, mis võtab sisendparameetrina vastu eesti isikukoodi
    // BigIntegrer kujul ja tagastab tõeväärtuse vastavalt sellele, kas kõnealuse isikukoodi kontrollnumber
    // on korrektne või mitte.
    // isikukoodi 

    public static boolean validatePersonalCode(BigInteger isikukood) {
        String isikukoodStr = isikukood.toString();
        int[] astmeKorrutis = {1, 2, 3, 4, 5, 6, 7, 8, 9, 1};
        int summa = 0;
        for (int jarg = 0; jarg < astmeKorrutis.length; jarg++) {
            String taht = isikukoodStr.substring(jarg, jarg + 1);
            int number = Integer.parseInt(taht); /// Itnegrer.parse teeb Stringist int
            summa = astmeKorrutis[jarg] * number + summa;

        }
        int kontrollnumber = summa % 11;
        if (kontrollnumber == 10) {
            throw new Error("Liiga keeruline");
        }


        int isikukoodInt = Integer.parseInt(isikukoodStr.substring(10, 11));

        if (isikukoodInt == kontrollnumber){
            return true;

        } else {
            return false;

        }



    }


}







