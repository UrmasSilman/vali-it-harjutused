public class Skydiver extends Athlete {
    public Skydiver(String perenimi) {
        super(perenimi);  // super  on konstruktor ja ütleb, et kasutada Athlete meetodit 'perform'
    }

    @Override
    public void perform() {
        System.out.println(perenimi + ": langen, langen, langen !!");
    }


}
