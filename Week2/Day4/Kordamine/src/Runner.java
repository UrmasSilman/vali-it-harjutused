public class Runner extends Athlete{
    public Runner(String perenimi) {
        super(perenimi);    // super  on konstruktor ja ütleb, et kasutada Athlete meetodit 'perform'
    }

    @Override
    public void perform() {
        System.out.println(perenimi +": Pikem samm, pikem samm!! ");
    }
}
