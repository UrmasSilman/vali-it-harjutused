import java.util.ArrayList;
import java.util.SortedMap;


/* Ülesanne 1:
a) Defineeri klass Sportlane (Athlete), millel järgmised
i) Omadused:
    1) Eesnimi
    2) Perenimi
    3) Vanus
    4) Sugu
    5) Pikkus
    6) Kaal
ii) Käitumine
1) Spordi (perform) - meetod mille kaudu teeks sportlane
seda, mis on tema spordiala
b) Defineeri kaks alamklassi, mis pärineksid Athlete klassis:
i) Skydiver
ii) Runner
c) Kirjuta üle (override’i) tuletatud klasside perform() meetod vastavalt
sellele, mis tüüpi sportlasega on tegemist.
d) Loo 3 objekti - 3 langevarjurit ja 3 jooksjat. Väärtusta nende
atribuutide väärtused.
e) Prindi need objektid ükshaaval standardväljundisse (atribuut
haaval).
f) Käivita sportlaste perform() meetodid main() meetodi seest

*/
    public class Main {

    public static void main(String[] args) {

        ArrayList<Athlete> sportlased = new ArrayList<>();
        sportlased.add(new Skydiver("Kukk"));
        sportlased.add(new Skydiver("Kana"));
        sportlased.add(new Skydiver("Hani"));
        sportlased.add(new Runner("Tiiger"));
        sportlased.add(new Runner("Puuma"));
        sportlased.add(new Runner("Gepard"));

        System.out.println("Hello World Greatest Athletes!");


        for (Athlete sportlane : sportlased) {
            System.out.println(sportlane.eesnimi); // sportlane.vanus sportlane.sugu jne
            System.out.println(sportlane.perenimi);
            System.out.println(sportlane.vanus);
            System.out.println(sportlane.sugu);
            System.out.println(sportlane.pikkus);
            System.out.println(sportlane.kaal); // sportlane.vanus sportlane.sugu jne

        }


    }
}
