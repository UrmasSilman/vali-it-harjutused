public abstract class Athlete {
    String eesnimi;
    String perenimi;
    int vanus;
    String sugu;
    double pikkus;
    double kaal;

    // see on meetod
    public abstract void perform() ; // abstract on vajalik kui tahad hiljem mujal 'Athlete' class extend kasutada
                                    // erinevate spordialade 'perform' funktsioooni kasutada
        // see on konstruktor
    public Athlete(String perenimi){  // Kui soovin lisada teised väljad, vanus, kaal jne, siis teen nede jaoks
        this.perenimi=perenimi;       // uued kosntruktorid

    }

}
