package ee.valiit.chat;

import java.util.ArrayList;

public class ChatRoom {
    public String room;
    public ArrayList<ChatMessage> messages = new ArrayList();

    public ChatRoom(String room) {
        this.room = room;  // this.room viitab klassile, String room rida 4
        messages.add(new ChatMessage("Juku", "Tsauki", "#"));
    }


    public void addMessage(ChatMessage msg) {
        messages.add(msg);
    }
}
