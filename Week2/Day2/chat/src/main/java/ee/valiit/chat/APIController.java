package ee.valiit.chat;

import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

// Iseseisev ülesanne:
        // 1. (lihtsam ) peale useri ja message lisa ka profiili pildi URL,
        // mis salvestub serverisse ja kuvatakse HTML'is välja. nimelt et
        // kasutaja saab kolmanda välja, kuhu see URL panna.
        // 2. Keerulisem ülesanne, loo uusi chat roome lisaks "general"ile.



@RestController // aitab ise kontrollida programmi korrektsust
@CrossOrigin    // lubab igat tüüpi päringuid

public class APIController {
    HashMap<String, ChatRoom> rooms = new HashMap();

    public APIController(){
        rooms.put("general", new ChatRoom("general"));
        rooms.put("random", new ChatRoom("random"));
        rooms.put("materjalid", new ChatRoom("materjalid"));
    }

    @GetMapping("/chat/{room}") // Kutsub välja messages
    ChatRoom chat (@PathVariable String room){
        return rooms.get(room);
    }

    @PostMapping("chat/{room}/new-message") // Postitab meassage
    void newMessage(@RequestBody ChatMessage msg, @PathVariable String room){
        rooms.get(room).addMessage(msg);

    }


}
