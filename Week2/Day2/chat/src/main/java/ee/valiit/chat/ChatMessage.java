package ee.valiit.chat;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.awt.*;
import java.util.Date;


public class ChatMessage {

    private String user;
    private String message;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    private Date date;
    private String avatar;


    public ChatMessage() {}

    public ChatMessage(String user, String message, String kuva) {
        this.user = user;
        this.message = message;
        this.date = new Date();
        this.avatar = avatar;
    }

    public String getUser() {   // get Loob uue User automaatselt ja annab selle väärtuse
        return user;
    }

    public String getMessage() {
        return message;
    }

    public Date getDate() {
        return date;
    }

    public String getAvatar(){
        return avatar;
    }


}

