
var url = "http://localhost:8080/list"
document.querySelector("#form1").onsubmit = async function(e){  //async
    console.log("onsubmit toimib")
    e.preventDefault() // sellekst et ei fresi automaatselt
    var nimi = document.querySelector("#nimi").value
    var vanus = document.querySelector("#vanus").value
    await fetch(url, {                                          //await
        method: "POST",
        body: JSON.stringify({nimi, vanus}),
        headers: {
        		'Accept': 'application/json',
        		'Content-Type': 'application/json'
        }
    })
    refreshTabel()
}
    // uus
    var refreshTabel = async function(){
        console.log("refreshTabel läks käima!")
        var andmed = await fetch(url)
        var isikuList = await andmed.json()
        console.log(isikuList)
        var isikuteHTML = ""
        for(var isik of isikuList){
            var nimi = isik.nimi
            var vanus = isik.vanus
            isikuteHTML += "Nimi: "+nimi+", vanus: "+vanus+"<br>"
        }
        document.querySelector("#koht").innerHTML = isikuteHTML

    }



    // fetch eesmärk - teha päringu ära