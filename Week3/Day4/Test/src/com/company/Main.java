package com.company;

import java.util.ArrayList;
import java.util.Arrays;

// Ül.1
//Deklareeri muutuja, mis hoiaks endas Pi (https://en.wikipedia.org/wiki/Pi) arvulist väärtust
// vähemalt 11 kohta peale koma.
//Korruta selle muutuja väärtus kahega ja prindi standardväljundisse.


public class Main {
    public static void main(String[] args) {

            double Pi = 3.14159265358;
            double PiKorrutis = Pi * 2;
            System.out.println(PiKorrutis);
        }


    // test Ül2.
    // Kirjuta meetod, mis tagastab boolean-tüüpi väärtuse ja mille sisendparameetriteks on
    // kaks täisarvulist muutujat.
    // Meetod tagastab tõeväärtuse vastavalt sellele,
    // kas kaks sisendparametrit on omavahel võrdsed või mitte.
    // Meetodi nime võid ise välja mõelda.

    //Kutsu see meetod main()-meetodist välja ja prindi tulemus standardväljundisse.

    public class Main2 {


        public boolean vordsed ( int x , int y ) {
            if ( x == y ) {
                return true;
            } else {
                return false;

            }

        }
    }



    // test Ül.3
    //Kirjuta meetod, mille sisendparameetriks on Stringide massiiv ja
    // mis tagastab täisarvude massiivi.
    // Tagastatava massiivi iga element sisaldab endas vastava sisendparameetrina
    // vastu võetud massiivi elemendi(stringi) pikkust.
    // Meetodi nime võid ise välja mõelda.
    // Kutsu see meetod main()-meetodist välja ja prindi tulemus (kõik massiivi elemendid)
    // standardväljundisse.



    public class Main3 {

        public void main ( String[] args) {
            ArrayList<String> stringid = new ArrayList<String>();

            stringid.add("Luule");
            stringid.add("Maali");
            stringid.add("Tiiu");

            for ( int j=0; j<stringid.size(); j++ )
                System.out.println("element " + j + ": " + stringid.get(j) );
        }

    }


    // Test Ül.4
    // Kirjuta meetod, mis võtab sisendparameetriks aastaarvu täisarvulisel kujul (int) vahemikus 1 - 2018
    // ja tagastab täisarvu (byte) vahemikus 1 - 21 vastavalt sellele,
    // mitmendasse sajandisse antud aasta kuulub.
    // Meetodi nime võid ise välja mõelda.
    // Kui funktsioonile antakse ette parameeter, mis on kas suurem,
    // kui 2018 või väiksem, kui 1, tuleb tagastada väärtus -1;
    //
    //Kutsu see meetod välja main()-meetodist järgmiste
    // erinevate sisendväärtustega: 0, 1, 128, 598, 1624, 1827, 1996, 2017.
    // Prindi need väärtused standardväljundisse.

    public class Main4 {
        //public static byte sajand (int aastaarv) {

        // Kui funktsioonile antakse ette parameeter, mis on kas suurem,
        // kui 2018 või väiksem, kui 1, tuleb tagastada väärtus -1;

        public void sajand(int aasta) {

            // No negative value is allow for year
            if (aasta <= 0 || aasta >= 2018 )
                System.out.print("-1");

                // If year is between 1 to 100 it
                // will come in 1st century
            else if (aasta <= 100)
                System.out.print("1ne sajand\n");

            else if (aasta % 100 == 0)
                System.out.print(aasta / 100 + " sajand");

            else
                System.out.print(aasta / 100 + 1 + " sajand");
        }

        // Driver code
        public void main(String[] args) {
            int aasta = 0;
            sajand(aasta);
        }
    }


    // Test Ül.5
    // Defineeri klass Country, millel oleksid meetodid
    // getPopulation, setPopulation, getName, setName ja list riigis enim kõneldavate keeltega.

    //Override’i selle klassi toString() meetod nii, et selle meetodi väljakutsumine tagastaks teksti,
    // mis sisaldaks endas kõiki parameetreid väljaprindituna.

    //Tekita antud klassist üks objekt ühe vabalt valitud riigi andmetega ja
    // prindi selle riigi info välja .toString() meetodi abil.


    public class Country {
        private int Population;
        private String Name;

        public Country() {

        }

        public int getPopulation () {
            return Population;
        }

        public void setPopulation ( int population ) {
            Population = population;
        }

        public String getName () {
            return Name;
        }

        public void setName ( String name ) {
            Name = name;
        }
    }

}




/* Test

Ülesanne 1:
Deklareeri muutuja, mis hoiaks endas Pi (https://en.wikipedia.org/wiki/Pi)
arvulist väärtust vähemalt 11 kohta peale koma.
Korruta selle muutuja väärtus kahega ja prindi standardväljundisse.

Ülesanne 2:
Kirjuta meetod, mis tagastab boolean-tüüpi väärtuse ja mille sisendparameetriteks
on kaks täisarvulist muutujat. Meetod tagastab tõeväärtuse vastavalt sellele,
kas kaks sisendparametrit on omavahel võrdsed või mitte.
Meetodi nime võid ise välja mõelda.

Kutsu see meetod main()-meetodist välja ja prindi tulemus standardväljundisse.

Ülesanne 3:
Kirjuta meetod, mille sisendparameetriks on Stringide massiiv ja mis tagastab
täisarvude massiivi. Tagastatava massiivi iga element sisaldab endas vastava
sisendparameetrina vastu võetud massiivi elemendi (stringi) pikkust.
Meetodi nime võid ise välja mõelda.

Kutsu see meetod main()-meetodist välja ja prindi tulemus (kõik massiivi elemendid) standardväljundisse.

Ülesanne 4:
Kirjuta meetod, mis võtab sisendparameetriks aastaarvu täisarvulisel kujul (int)
vahemikus 1 - 2018 ja tagastab täisarvu (byte) vahemikus 1 - 21 vastavalt sellele,
mitmendasse sajandisse antud aasta kuulub.
Meetodi nime võid ise välja mõelda. Kui funktsioonile antakse ette parameeter,
mis on kas suurem, kui 2018 või väiksem, kui 1, tuleb tagastada väärtus -1;

Kutsu see meetod välja main()-meetodist järgmiste erinevate
sisendväärtustega: 0, 1, 128, 598, 1624, 1827, 1996, 2017.
Prindi need väärtused standardväljundisse.

Ülesanne 5:
Defineeri klass Country, millel oleksid meetodid
getPopulation, setPopulation, getName, setName
ja list riigis enim kõneldavate keeltega.

Override’i selle klassi toString() meetod nii,
et selle meetodi väljakutsumine tagastaks teksti,
mis sisaldaks endas kõiki parameetreid väljaprindituna.

Tekita antud klassist üks objekt ühe vabalt valitud riigi andmetega
ja prindi selle riigi info välja .toString() meetodi abil.


*/




