console.log("Tere hommikut!")

var sisend = 9
var tulemus

// kui on vähem kui 7, kooruta kahega
// kui on suurem kui 7, jaga kahega
// Kui on täpselt 7, jäta samaks

if ( sisend == 7 ) {
	tulemus = sisend 
} else if ( sisend < 7) {
	tulemus = sisend * 2
} else {
	tulemus = sisend / 2
}

console.log("Tulemus on: " + tulemus )

/* Kui sõned on võrdsed, siis prindi üks(console.log),
	kui erinevad, siis liida kokku ja prindi.
	*/
	var str1 = "banaan"
	var str2 = "apelsin"

	if (str1 == str2) {
		console.log(str1)
	} else {
		console.log(str1 + " " + str2) 
	}


/*	Meil on linnade nimekiri, aga ilma sõnata "linn". 
	Need palun lisada.
	*/
	var linnad = ["Tallinn", "Tartu", "Valga"]  // Defineerime nimekirja
	var uuedLinnad = [] // Defineerime nimekirja kuhu tulemused panna

	while (linnad.length > 0) { // Kuni linnasid on listis rohkem kui 0
		var linn = linnad.pop() // võta välja viimane
		var uusLinn = linn + " linn" // ja lisa "linn" otsa
		uuedLinnad.push(uusLinn) // tulemus salvesta uude listi
	}

	console.log(uuedLinnad)


/*
	Eralda poiste ja tüdrukute nimed
	*/

	var nimed = ["Margarita", "Mara", "Martin", "Kalev"]
	var poisteNimed = []
	var tydrukuteNimed = []

	while (nimed.length > 0) {
	// "a" lõpuga on tüdruku nime. Googelda kuidas seda küsida.
	// Kui on poisi nimi, lisa see poisteNimed listi
	// Kui on tüdruku nimi. lis see tydrukuteNimed listi.
	var nimi = nimed.pop()
	if (nimi.endsWith("a")) {
		tydrukuteNimed.push(nimi)

	} else {
		poisteNimed.push(nimi)
	}
}
console.log(poisteNimed, tydrukuteNimed)


// ctrl + klikk saad mitu kursorit
// ctrl + enter tekitab uue rea
// console.log("muutuja", muutuja) prindib

/* Funktsioonid */

/* 
	Kirjuta algoritm, mis suudab teha tüdrukute ja poiste nimede eristust
*/ 

	var eristaja = function(nimi) {
		if (nimi.endsWith("a")) {
			return "tüdruk"
		} else {
			return "poiss"
		}
	}

console.log(eristaja("Peeter"))
console.log(eristaja("Margarita"))
console.log(eristaja("Krister"))


	
	var praeguneNimi = "Peeter"
	var kumb = eristaja("Peeter")
	console.log(kumb)

/*
	Loo funktsion, mis tagastab vastuse küsimusele
	kas tegu on numbriga?
	!isNaN(4) // is Not a Number. 
	Hüüumärk muudab true/false vastupidiseks
	*/

	var kasOnNumber = function(number) {
		if (!isNaN (number)) {
			return true
		}
		return false
	}

	console.log( kasOnNumber (4))
	console.log( kasOnNumber ("mingi sõne"))
	console.log( kasOnNumber (23456))
	console.log( kasOnNumber (6.876))
	console.log( kasOnNumber (null))
	console.log( kasOnNumber ([1, 4, 5, 6]))


/* 	Kirjuta funktsioon, mis võtab vastu kaks numbrit
	ja tagatab nende summa
	*/


	var summa = function(a, b) {
		return  a + b		
	}

console.log( summa (4, -5))
console.log( summa (9, 67))

/*

*/  

console.log ("----------------------")


var inimesed = {
	Kaarel: 34,
	"Margarita": 10,
	"Suksu": [3, 4, 5],
	"Krister": {
		vanus: 30,
		sugu: true
	}
}

console.log(inimesed["kaarel"])
console.log(inimesed.Kaarel)
console.log(inimesed.Margarita)
console.log(inimesed.Suksu[1])
console.log(inimesed.Krister.sugu)





