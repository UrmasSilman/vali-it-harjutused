console.log("Töötan!")

// Ul: Laadida alla API'st tekst

var refreshMessages = async function() {
	// lihtsalt selleks, et tean ,et kood läks käima
	console.log("refresh.Messages läks käima Urmas")

	// API aadress on string, salvestan lihtsalt muutujasse
	var APIurl = "http://138.197.191.73:8080/chat/general"
	// fetch teeb päringu serverisse (meie defineeritud aadressil)
	var request = await fetch(APIurl)
	// json() käsk vormindab kasutatavasse formaati JSONiks
	var json = await request.json()
	console.log("json")
	
	// Kuva serverist saadud info HTMLis (ehk brauseri lehel)
	document.querySelector('#jutt').innerHTML = ""
	var sonumid = json.messages
	while (sonumid.length > 0) { // Kuniks sõnumeid on
	// while (json.messages.lenght > 0) // Sama tähendusega, mis eelmine
		var sonum = sonumid.shift()

		// Lisa HTML #jutt sisse sonum.message	
		document.querySelector('#jutt').innerHTML += 
		"<p>" + sonum.user +": " + sonum.message + "</p>"

	}
	// scrolli kõige alla 
	window.scrollTo(0,document.body.scrollHeight);
	
}
// Uuenda sõnumeid iga 5 sekundi järel
setInterval(refreshMessages, 5000)
refreshMessages()

document.querySelector('form').onsubmit = function(event){
	event.preventDefault()
	console.log("submit käivitus")

	// Korjame kokku formist info

	var username = document.querySelector('#username').value
	var message = document.querySelector('#message').value
	document.querySelector('#message').value = "" // tee input tühjaks
	console.log(username, message)

					// see on serveri poolt antud URL
	var APIurl = "http://138.197.191.73:8080/chat/general/new-message"
	
							
	fetch(APIurl, {
		method: "POST",  // POST päring postitab uue andmetüki serverisse
		body: JSON.stringify({user: username, message: message}),
		headers:{
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		}
	})

}


