public class Main {

    public static void main(String[] args) {

        System.out.println("Hello World!");
        System.out.println("sout"); // sout on kiirkäsk
        ; // iga rea lõpus peab olema semikoolon
        int number = 5;
        double n2 = 5.7;
        // byte bait =5;
        // 8 bit = = 1 byte
        System.out.println(200 - 110);

        String nimi = "Urmas"; // RAM saab info {Value: Urmas  Length: 5}
        char algusTaht = 'U';

        if (number == 5) { // if süntaks on täpselt sama, mis JSis
        } else {
        }
        String liitmine = 5 + " tere";

        int liitmine2 = (int) (number + n2); // cast-in ühest tüübist teise
        System.out.println(liitmine);
        System.out.println(Math.round(10.7)); // ümardamiskes on erladi meetod


        int parisNumber = Integer.parseInt("4");
        double parisNumber2 = Double.parseDouble("4.1");

        String puuvili1 = "Banaan";
        String puuvili2 = "Banaan";
        if (puuvili1 == puuvili2) {
            System.out.println("Puuviljad on võrdsed !!!");
        } else {
            System.out.println("Ei ole võrdsed !");
        }

        Koer.auh();
        Koer.lausu();
        Kass.nurr();

    }
    public static int summa(int a, int b) {
        return a + b;
    }

}

