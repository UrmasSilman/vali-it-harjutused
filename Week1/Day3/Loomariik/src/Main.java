public class Main {

    // lühend "psvm" public static void main(String[] args)
    public static void main(String[] args) {
        Koer pontu = new Koer();
        pontu.lausu();
        pontu.maga();
        pontu.ärata();

        Kass nurr = new Kass();
        nurr.lausu();
        nurr.maga();
        nurr.ärata();

        String saba = nurr.getSaba();
        System.out.println(saba);

        String saba2 = pontu.getSaba();
        System.out.println(saba2);

    }
}
