import java.util.ArrayList;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        // Keskseks teemaks on array ehk massiiv

        int[] massiiv = new int[6];
        ArrayList list = new ArrayList();

        // Ül1: prindi välja massiv
        String massiivStr = Arrays.toString(massiiv);
        System.out.println(massiivStr);


        // Ül2: Muuda kolmandal positsioonil olev number viieks
        massiiv[2] = 5;
        System.out.println(Arrays.toString(massiiv));


        // Ül3:Prindi välja kuues element massiivist(aga enne määra talle väärtus)
        massiiv[5] = 13;
        System.out.println(massiiv[5]);

        // Ül4: Prindi välja viimane element, ükskõik kui pikk massiiv ka ei oleks
        int viimane = massiiv[massiiv.length - 1];
        System.out.println("Viimane " + viimane);

        //ÜL4: Loo uus massiiv, kus on kõik 8 numbrit kohe alguses määratud
        int[] massiiv2 = new int[]{1, 2, 3, 4, 55, 6, 7, 99};
        System.out.println(Arrays.toString(massiiv2));

        //Ül5: Prindi välja ükshaaval kõik väärtused massiiv2-st
        // Tavaline tsükkel
        int index = 0;
        while (index < massiiv2.length) {
            System.out.println(massiiv2[index]);
            index++;
        }

        //ÜL6: Teeme sama tsükli kiiremini kasutades "for" tsüklit
        // Index-iga tsükkel
        for (int i = 0; i < massiiv2.length; i++) {
            System.out.println(massiiv2[i]);
        }

        //ÜL7: 1.Loo Stringide massiiv, mis on alguses tühi
        //     2. ja siis lisad ka veel keskele mõne sõne

        // 1. Loos Stringide massiiv, mis on alguses tühi
        String[] stringid = new String[3];
        System.out.println(stringid);
        // 2. ja siis lisad ka veel keskele mõne sõne
        stringid[1] = "Kalevipoeg";
        System.out.println(Arrays.toString(stringid));


        //ÜL8 1. Loo massiiv, kus on sada kohta
        //    2. Täida massiiv järjest loeteluga alustades numbrist 0
        //    3. Prindi välja see mega massiiv

        int[] sadakohta = new int[100];
        for (int i = 0; i < sadakohta.length; i++) {
            sadakohta[i] = i;
        }
            System.out.println(Arrays.toString(sadakohta)); // trükib välja numbrid nullist 99ni samal real

        int[] sadakohta1 = new int[100];
        for (int i = 0; i < sadakohta1.length; i++) {
            System.out.println(i); // trükib välja numbrid nullist 99ni iga number uuel real
        }


        //ÜL9  1. Kasuta sadakohta massiivi, kus on numbrite jada
        //     2. loe, mitu paarisrvu on?
        //     3. Prindi tulemus välja
        //     Kasutada tuleb nii tsüklit kui ka if lauset
        //     Kui jagad kaks arvu operaatoriga %, siis see tagastab jäägi
        //     x % 2 tähendab, et = 0 puhul on paarisarv ja 1 puhul ei ole

        // Tegevus on järgmine;
        // 1. Võtan esimese numbri massiivist. See on null
        // 2. Ma küsin, kas see number on paaris või mitte? On
        // 3. Kuna on paaris, siis liidan loendajale otsa

        // Algoritm on järgmine
        // 1. Võtan esimese numbri massiivist. 0
        // 2. Kui number on paaris?
        // 3. Siis lisan ühe loendajale otsa

        int mituPaarisarvuOn = 0;
        for (int i = 0; i < sadakohta.length; i++) {
            if (i % 2 == 0) {
                mituPaarisarvuOn++;
            }


        }
        System.out.println("On nii palju paarisarve: " + mituPaarisarvuOn);


        //ÜL10 Loo ArrayList ja sisesta sinna kolm numbrit ja kaks Stringi. ArrayList saab sisestada nii Stringi
        //     kui ka numbreid

        ArrayList nimekiri  = new ArrayList();
        nimekiri.add("Siga");
        nimekiri.add("Kägu");
        nimekiri.add("Mats");
        nimekiri.add(31);
        nimekiri.add(22);
        System.out.println(nimekiri);

        //Ül10  Küsi viimasest ülesandest nimekiri välja kolmas element ja prindi välja
        System.out.println(nimekiri.get(2));

        //ÜL11  Prindi kogu list välja.
        System.out.println(nimekiri);

        //Ül.12 Prindi iga element ükshaaval välja. Vaja on while või for tsüklit
        for(int i=0; i < nimekiri.size(); i++) {
            System.out.println(nimekiri.get(i));
        }

        //ÜL13 1. Loo uus ArrayList, kus on näiteks 543 numbrit.
            // 1.1 Numbrid peavad olema suvalised. Nimelt Math.random() vahemikus 0-10.
            // 2. Korruta iga number viiega.
            // 3. Salvesta see uus number samle positsioonile.

            // for (int i=0; i <100; i++) { // see on vaid vihje
            //   double nr = Math.random() * 10;
            //    System.out.println(nr);


        // Uks versioon
        ArrayList massiiv543  = new ArrayList();
        for(int i=0; i < 543; i++) {
            int nr = (int) (Math.random() * 11);
            massiiv543.add(nr);
        }
        System.out.println(massiiv543);

        // Teine versioon seda teha
        ArrayList massiiv543_2  = new ArrayList();
        for(int i=0; i < 543; i++) {
            double nr = Math.floor(Math.random() * 11);
            massiiv543_2.add(nr);

        }
        System.out.println("Algne massiiv: " + massiiv543_2);

        // korrutame iga numbri viiega ja lisab samasse kohta
        for(int i=0; i < 543; i++) {
            double nr = (double) massiiv543_2.get(i);
            double muudetudNr = nr * 5;
            massiiv543_2.set(i, muudetudNr);
        }
        System.out.println("Muudetud masssiv *5: " + massiiv543_2);

        // veel üks võimalus
        for(int i=0; i < massiiv543_2.size(); i++) {
            double nr = (double) massiiv543_2.get(i);
            double muudetudNr1 = nr * 5;
            massiiv543_2.set(i, muudetudNr1);
        }
        System.out.println("Muudetud masssiv *5: " + massiiv543_2);
    }

}
