import nadalapaevad.Reede;

public class Main {

    public static void main(String[] args) {
        System.out.println("hei!");


        // boolean harjutused

        if(false && true ||  !(1>0) && true){ // true ja false  on boolen tüüpi
            System.out.println("Tõene");
        } else {
            System.out.println("Väär");
        }



        if(false || false && true || true){ // true ja false  on boolen tüüpi
            System.out.println("Tõene");
        } else {
            System.out.println("Väär");
        }



        if((false && true) || true){ // true ja false  on boolen tüüpi
            System.out.println("Tõene");
        } else {
            System.out.println("Väär");
        }

        Reede.koju();
        // reede on klass, koju on meetod
        // klassi ja meetodi loomise shortcut:
        // kliki peale ja Alt+Enter

        Laupaev.peole();

        Pyhapaev paev = new Pyhapaev();
        paev.maga();
        paev.uni();





    }
}
