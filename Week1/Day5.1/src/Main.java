import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
        System.out.println("hei!");

        //Ül1: 1. Loo kolm muutujat numbritega
        //     2. Moodusta lause nende muutujatega
        //     3. prinndi see laise välja

        int aasta = 70;
        int kuu = 2;
        int paev = 7;

        String lause = "Mina sündisin aastal  " + aasta + ". Kuu oli " +kuu+" ja kuupäev " +paev+ ".";
        System.out.println(lause);

        // %d on täisarv
        // %f on komakohaga
        // %s on string

        String paremlause = String.format(" Mina sündisin aastal %d. Kuu oli %d ja kuupäev %d...", aasta , kuu, paev);
        System.out.println(paremlause);

        //Ül2. 1. Loe kooku mitu lampi on klassis ja pane see info HashMappi!
        //     2. Loe kokku mitu akent on klassis ja pane see info HashMappi!
        //     3. Loe kokku mkitu inimest on klassis ja pane see info HashMappi!

        // !! HashMapi välja trükitud järjekord on suvaline, mitte sisestuse järgjestuses!!!
        HashMap klassiAsjad = new HashMap();
        klassiAsjad.put ("aknad", 5);
        klassiAsjad.put ("lambid", 8);
        klassiAsjad.put("inimesed", 21);
        klassiAsjad.put ("lambid", 15); // Kui sama sisend on , siis võtab suurima väärtuse

        System.out.println(klassiAsjad);

        //Mõtle välja 3 praktilist rakendust, kus on HasMapi parem kasutada(!!Listis enam teha ei saa!!)
        // !! HasMapi on hea kasutada siis kui sa soovid kasuada ka mingit sõnet, mitte ainult indekseid

        // 1. Haiglas on {patsiendiID: nimi}
        // 2. Trammis on {reoheliseKaardiID: kontojääk}
        // 3. Laual on {L:5mm, M:3mm, F:1mm} markerid
        // 4. Riiuilis on {mituAsja: arv}
        // 5. Valimistel on {mituHäältErakonnal: arv}

        //Ül3. Prindi välja kui palju on inimesi klassis, kasutades juba loodud HashMappi
        HashMap inimesedKlassis = new HashMap();
        inimesedKlassis.put ("inimest on klassis", 21);
        System.out.println(inimesedKlassis);

        int inimesi = (int)klassiAsjad.get("inimesed");
        System.out.println(inimesi);

        // saab ka nii sama tulemuse
        System.out.println(klassiAsjad.get("inimesed"));

        // Lisa samassew HashMappi juurde mitu tasapinda on klassis, enne number, siis string

        klassiAsjad.put(10, "tasapinda");
        System.out.println(klassiAsjad);

        //Ül4. Loo uus HashMap, kuhu saab sisetada AINULT {String:double} paare. Sisesta sinna ka midagi
        HashMap<String, Double> tyybitudHashMap = new HashMap<>();
        tyybitudHashMap.put("Number", 2.0);
        System.out.println(tyybitudHashMap);

        //Ül5. switch
        int rongiNr = 50;
        String suund = null;
        switch (rongiNr) {
            case 50: suund = "Pärnu"; break;
            case 51: suund = "Rakvere";
            case 55: suund = "Haapsalu"; break;
            case 10: suund= "Vormsi"; break;

        }
        System.out.println(suund);

        int rongiNr1 = 50;
        String suund1 = null;
        switch (rongiNr1) {
            case 50: suund1 = "Pärnu";
            case 51: suund1 = "Rakvere";
            case 55: suund1 = "Haapsalu";break;
            case 10: suund1 = "Vormsi";
        }
        System.out.println(suund1); // Trükib välja viimase tulemuse. Prindi käsk on protsessist väljas



        //Ül6. FOREACH
        int[] mingidNumbrid = new int[] {8, 4, 6, 345, 8976, 12345667};
        for(int i = 0; i < mingidNumbrid.length; i++) {

        System.out.println(mingidNumbrid[i]); // Trükib välja kõik tulemused. Prindi käsk on protsessi sees
        }

        System.out.println("------------------");
        for(int nr : mingidNumbrid) {
            System.out.println(nr);

        }

        //Ül.7. Õpilane saab töös punkte 0-100
        //      1. kui punkte on alla 50, kukub ta läbi
        //      2. vastasel juhul on hinne täisrvuline punktid/20
        //      100 punkti => 5
        //      80 punkti => 4
        //      67 punkti => 3
        //      50 punkti => 2

        int punkte = 20;
        if(punkte > 100 || punkte <0) {
            System.out.println("See ei ole õige number. Palun sisesta punktid uuesti korrektselt.");

        }

        switch ((int) Math.round(punkte/20.0)) {
            case 5:
                System.out.println("suurepärane");
                break;
            case 4:
                System.out.println("hea");
                break;
            case 3:
                System.out.println("rahuldav");
                break;
            case 2:
                System.out.println("ee... kas sa õppisid ka?");
                default:
                    System.out.println("Kukkusid läbi");



        }

    }

}
